<?php

namespace Tests\Jwt;

/**
 * Class ParserTest
 * @package Tests\Ds\Jwt
 */
class ParserTest  extends \PHPUnit_Framework_TestCase{

    public function testImport(){}
    public function testImportPartMissing(){}

    public function testProcessHeader(){}
    public function testProcessHeaderNoType(){}
    public function testProcessHeaderTypeInvalid(){}
    public function testProcessHeaderNoAlg(){}
    public function testProcessHeaderAlgNotValid(){}

    public function testGetHeader(){}
    public function testGetHeaders(){}

    public function testGetClaim(){}
    public function testGetClaims(){}

    public function testGetSignature(){}
    public function testValidateClaim(){}
    public function testValidateClaimMissingRule(){}
    public function testValidateClaimNotMatching(){}

    public function testProcessPayload(){}
    public function testProcessPayloadException(){}
    public function testCheckTotalClaims(){}
    public function testCheckTotalClaimsException(){}
    public function testCheckNotBefore(){}
    public function testCheckNotBeforeException(){}
    public function testCheckExpires(){}
    public function testCheckExpiresException(){}
}
