<?php

namespace Tests\Jwt;

use Rs\Jwt\Validator;

/**
 * Class ValidatorTest
 * @package Tests\Jwt
 */
class ValidatorTest  extends \PHPUnit_Framework_TestCase{

    /**
     * @var Validator
     */
    public $validator;

    /**
     * set up
     */
    public function setUp(){
        $this->validator = new Validator();
    }

    public function testGetRules(){
        $expected = [
            'claim-1' => 'claim',
            'claim-2' => 'value'
        ];

        $valWithClaim = $this->validator->withClaim('claim-1','claim');
        $valWithTwoClaims = $valWithClaim->withClaim('claim-2', 'value');
        $this->assertEquals($valWithTwoClaims->getRules(), $expected);
    }

    public function testGetRule(){
        $valWithClaim = $this->validator->withClaim('claim-1',['claim']);
        $this->assertEquals($valWithClaim->getRules(), ['claim-1' => ['claim']]);
    }

    public function testWithId(){
        $valWithClaim = $this->validator->withId('my-id');
        $this->assertEquals($valWithClaim->getRule('jti'), 'my-id');
    }

    public function testWithIssuer(){
        $valWithClaim = $this->validator->withIssuer('my-issuer');
        $this->assertEquals($valWithClaim->getRule('iss'), 'my-issuer');
    }

    public function testWithAudience(){
        $valWithClaim = $this->validator->withAudience('my-audience');
        $this->assertEquals($valWithClaim->getRule('aud'), 'my-audience');
    }

    public function testWithSubject(){
        $valWithClaim = $this->validator->withSubject(['subject-key' => 'subject-value']);
        $this->assertEquals($valWithClaim->getRule('sub'), ['subject-key' => 'subject-value']);
    }

    public function testWithNotBefore(){
        $timestamp = time();
        $valWithClaim = $this->validator->withNotBefore($timestamp);
        $this->assertEquals($valWithClaim->getRule('nbf'), $timestamp);
    }

    public function testWithExpires(){
        $valWithClaim = $this->validator->withIssuer('my-issuer');
        $this->assertEquals($valWithClaim->getRule('iss'), 'my-issuer');
    }

    public function testWithIssuedAt(){
        $valWithClaim = $this->validator->withIssuer('my-issuer');
        $this->assertEquals($valWithClaim->getRule('iss'), 'my-issuer');
    }

    public function testWithClaim(){
        $valWithClaim = $this->validator->withIssuer('my-issuer');
        $this->assertEquals($valWithClaim->getRule('iss'), 'my-issuer');
    }

    public function testGetTotalRules(){
        $valWithClaim = $this->validator->withIssuer('my-issuer');
        $this->assertEquals($valWithClaim->getRule('iss'), 'my-issuer');
    }
}
