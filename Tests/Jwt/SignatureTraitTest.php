<?php

namespace Tests\Jwt;

use Rs\Jwt\SignatureTrait;

/**
 * Class TraitTestClass
 * @package Tests\Ds\Jwt
 */
class TraitTestClass{
    use SignatureTrait;
}

/**
 * Class SignatureTraitTest
 * @package Tests\Ds\Jwt
 */
class SignatureTraitTest  extends \PHPUnit_Framework_TestCase{

    /**
     * @var JwtTrait
     */
    public $jwtTraitClass;

    public function setUp(){
        $this->jwtTraitClass = new TraitTestClass();
    }

    /**
     * Test that an array matches nase64 encoded string.
     */
    public function testPack(){
        $input = ['foo', 'bar'];
        $expected = 'WyJmb28iLCJiYXIiXQ==';
        $actual = $this->jwtTraitClass->pack($input);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that base64 encoded string matches array.
     */
    public function testUnpack(){
        $input = 'WyJmb28iLCJiYXIiXQ==';
        $expected = ['foo', 'bar'];
        $actual = $this->jwtTraitClass->unpack($input);
        $this->assertEquals($expected, $actual);
    }
}
