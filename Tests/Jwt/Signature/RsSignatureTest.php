<?php

namespace Tests\Jwt\Signature;

use Rs\Jwt\Parser;
use Rs\Jwt\Signature\RsSignature;

/**
 * Class RsSignatureTest
 * @package Tests\Ds\Jwt
 */
class RsSignatureTest  extends \PHPUnit_Framework_TestCase{

    /**
     * @var string
     */
    public $publicKey;

    /**
     * @var string
     */
    public $privateKey;

    /**
     * @var RsSignature
     */
    public $signature;


    public function setUp(){
        $this->publicKey = file_get_contents(dirname(__DIR__) . '/keys/key.pub');
        $this->privateKey = file_get_contents(dirname(__DIR__) . '/keys/key.priv');
        $this->signature = new RsSignature($this->publicKey, $this->privateKey);
        $this->parser = $this->getMockBuilder('\Rs\Jwt\Parser')->getMock();
    }

    public function testSign(){

        $header = [
            "alg" => "RS256",
            "typ" => "JWT"
        ];
        $payload = ['claim' => 'my-claim'];
        $expected = $this->createSignature($header, $payload);
        $actual = $this->signature->sign($payload);


        $this->assertEquals($expected, $actual);
    }

    public function testVerify(){
        $header = [
            "alg" => "RS256",
            "typ" => "JWT"
        ];
        $payload = ['claim' => 'my-claim'];
        $signature = $this->createSignature($header, $payload);

        $this->parser->expects($this->at(0))
            ->method('getHeaders')
            ->willReturn(
                $header
            );

        $this->parser->expects($this->at(1))
            ->method('getPayload')
            ->willReturn(
                $payload
            );

        $this->parser->expects($this->at(2))
            ->method('getSignature')
            ->willReturn(
                $signature
            );

        $result = $this->signature->verify($this->parser);
        $this->assertEquals($payload, $result);
    }


    public function testVerifyInvalidPayload(){
        $header = [
            "alg" => "RS256",
            "typ" => "JWT"
        ];
        $payload = ['claim' => 'my-claim'];
        $signature = $this->createSignature($header, $payload);

        $this->parser->expects($this->at(0))
            ->method('getHeaders')
            ->willReturn(
                $header
            );

        $this->parser->expects($this->at(1))
            ->method('getPayload')
            ->willReturn(
                null
            );

        $this->setExpectedException(\Exception::class,'Payload hash is invalid.');

        $result = $this->signature->verify($this->parser);
        $this->assertEquals($payload, $result);
    }


    public function createSignature($header, $payload){

        $data = base64_encode(json_encode($header)).base64_encode(json_encode($payload));

        \openssl_sign(
            $data,
            $signature,
            $this->privateKey,
            OPENSSL_ALGO_SHA256
        );
        return $signature;
    }
}