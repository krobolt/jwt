<?php

namespace Tests\Jwt\Signature;

use Rs\Jwt\Parser;
use Rs\Jwt\Signature\HsSignature;

/**
 * Class HsSignatureTest
 *
 * @package Tests\Ds\Jwt
 */
class HsSignatureTest  extends \PHPUnit_Framework_TestCase
{

    public $secret;

    /**
     * @var HsSignature
     */
    public $signature;

    public $parser;


    public function setUp()
    {
        $this->secret = 'my-secret';
        $this->signature = new HsSignature($this->secret);
        $this->parser = $this->getMockBuilder('\Rs\Jwt\Parser')->getMock();
    }


    public function testSign()
    {
        $header = [
            "alg" => "HS256",
            "typ" => "JWT"
        ];

        $payload = ['claim' => 'my-claim'];

        $data = base64_encode(json_encode($header)).base64_encode(json_encode($payload));
        $expected = hash_hmac('sha256', $data, $this->secret);

        $actual = $this->signature->sign($payload);

        $this->assertEquals($expected, $actual);
    }

    public function testVerify()
    {
        $payload = ['claim' => 'my-claim'];
        $sig =  $this->signature->sign($payload);

        $this->parser->expects($this->at(0))
            ->method('getPayload')
            ->willReturn(
                $payload
            );

        $this->parser->expects($this->at(1))
            ->method('getSignature')
            ->willReturn(
                $sig
            );

        $result = $this->signature->verify($this->parser);
        $this->assertEquals($payload, $result);
    }

    public function testVerifyExpectionInvalidHash()
    {
        $this->setExpectedException(\Exception::class, 'Payload hash is invalid.');
        $this->signature->verify($this->parser);
    }

    public function testVerifyExpectionSignatureMissing()
    {
        $payload = ['claim' => 'my-claim'];
        $this->parser->expects($this->at(0))
            ->method('getPayload')
            ->willReturn(
                $payload
            );

        $this->setExpectedException(\Exception::class, 'Signature missing.');
        $this->signature->verify($this->parser);
    }

    public function testVerifyExpectionSignatureValid()
    {
        $payload = ['claim' => 'my-claim'];
        $this->parser->expects($this->at(0))
            ->method('getPayload')
            ->willReturn(
                $payload
            );

        $this->parser->expects($this->at(1))
            ->method('getSignature')
            ->willReturn(
                'somerandom820193890123sig'
            );

        $this->setExpectedException(\Exception::class, 'Signature not valid.');
        $this->signature->verify($this->parser);
    }
}
