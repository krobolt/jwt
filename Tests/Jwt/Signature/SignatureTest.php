<?php

namespace Tests\Jwt\Signature;

use Rs\Jwt\ParserInterface;
use Rs\Jwt\Signature\Signature;
use Rs\Jwt\SignatureInterface;

class MockSignature extends Signature implements SignatureInterface{

    public $header = [
        'typ' => "JWT"
    ];

    public $algos = [
        'algo' => 'algo_value',
        'algo2' => 'algo_value2'
    ];

    public function sign(array $payload)
    {
        return true;
    }


    public function verify(ParserInterface $parser)
    {
        return true;
    }
}

/**
 * Class SignatureTest
 * @package Tests\Ds\Jwt
 */
class SignatureTest  extends \PHPUnit_Framework_TestCase{

    public $signature;

    public function setUp(){
        $this->signature = new MockSignature();
    }

    public function testGetHeader()
    {
        $expected = [
            'typ' => "JWT"
        ];
        $actual = $this->signature->getHeader();
        $this->assertEquals($actual, $expected);
    }

    public function testSetHeader()
    {
        $expected = [
            'typ' => "my-type"
        ];
        $this->signature->setHeader('typ',"my-type");
        $actual = $this->signature->getHeader();

        $this->assertEquals($actual, $expected);
    }

    public function testGetAlgos()
    {
        $expected = [
            'algo' => 'algo_value',
            'algo2' => 'algo_value2'
        ];
        $actual = $this->signature->getAlgos();
        $this->assertEquals($actual, $expected);
    }
}
