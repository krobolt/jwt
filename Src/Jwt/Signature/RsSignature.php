<?php

namespace Rs\Jwt\Signature;

use Rs\Jwt\ParserInterface;
use Rs\Jwt\SignatureInterface;
use Exception;

/**
 * Class RsSignature
 * @package Rs\Jwt\Signature
 */
class RsSignature extends Signature implements SignatureInterface
{
    /**
     * @var array
     */
    protected $header = [
        "alg" => "RS256",
        "typ" => "JWT"
    ];

    /**
     * Active open SSL algorithm constant.
     * @var int
     */
    protected $active = OPENSSL_ALGO_SHA256;

    /**
     * Open SSL algorithm constants.
     * @var array
     */
    protected $algos = [
        "RS256" => OPENSSL_ALGO_SHA256,
        "RS384" => OPENSSL_ALGO_SHA384,
        "RS512" => OPENSSL_ALGO_SHA512,
    ];

    /**
     * RSA private key.
     * @var string
     */
    private $privateKey;

    /**
     * RSA public key.
     * @var string
     */
    private $publicKey;

    /**
     * RsSignature constructor.
     * @param string $publicKey
     * @param string $privateKey
     * @throws Exception
     */
    public function __construct($publicKey, $privateKey = '')
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;

        if (!function_exists('openssl_verify')){
            throw new Exception('missing openssl extension.');
        }

    }

    /**
     * Return signed signature.
     * @param array $payload
     * @return string
     * @throws \Exception
     */
    public function sign(array $payload){
        $binary_signature = null;
        \openssl_sign(
            $this->pack($this->header).$this->pack($payload),
            $binary_signature,
            $this->privateKey,
            $this->active
        );

        if ($binary_signature === null || !is_string($binary_signature)){
            throw new Exception('Unable to create binary signature.');
        }

        return $binary_signature;
    }

    /**
     * Verify against payload.
     *
     * @param ParserInterface $parser
     * @return mixed
     * @throws \Exception
     */
    public function verify(ParserInterface $parser){

        $providedHeader = $parser->getHeaders();
        $providedPayload = $parser->getPayload();
        $providedSignature = $parser->getSignature();

        $parser->verifyPayload();

        if (!is_array($providedPayload)){
            throw new Exception('Payload hash is invalid.');
        }

        $success = \openssl_verify(
            $this->pack($providedHeader).$this->pack($providedPayload),
            $providedSignature,
            $this->publicKey,
            $this->active
        );

        if ($success === -1) {
            throw new \Exception("openssl_verify() failed with error.  " . \openssl_error_string() . "\n");
        }

        if ($success === 1) {
            return $providedPayload;
        }

        throw new Exception("Signature verification failed. Incorrect key or data has been tampered with!\n");
    }
}
