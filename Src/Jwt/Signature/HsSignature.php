<?php

namespace Rs\Jwt\Signature;

use Rs\Jwt\ParserInterface;
use Rs\Jwt\SignatureInterface;

/**
 * Class HsSignature
 * @package Rs\Jwt\Signature
 */
class HsSignature extends Signature implements SignatureInterface
{
    /**
     * Signature token header.
     * @var array
     */
    protected $header = [
        "alg" => "HS256",
        "typ" => "JWT"
    ];

    /**
     * Collection of algorithm types.
     * @var array
     */
    protected $algos = [
        "HS256" => 'sha256',
        "HS384" => 'sha384',
        "HS512" => 'sha512',
    ];

    /**
     * Active algorithm
     * @var string
     */
    protected $active = 'sha256';


    /**
     * Secret hash key.
     * @var string
     */
    private $secret;

    /**
     * HsSignature constructor.
     * @param string $secret
     */
    public function __construct($secret)
    {
        $this->secret = $secret;
    }

    /**
     * Return signed hmac signature.
     * @param array $payload
     * @return string
     * @throws \Exception
     */
    public function sign(array $payload){
        $signature = \hash_hmac(
            $this->active,
            $this->pack($this->header).$this->pack($payload),
            $this->secret
        );
        if ($signature === null || !is_string($signature)){
            throw new \Exception('Unable to create hmac signature.');
        }

        return $signature;
    }

    /**
     * Verify unknown signature string against payload.
     *
     * @param ParserInterface $parser
     * @return mixed
     * @throws \Exception
     */
    public function verify(ParserInterface $parser){

        $providedPayload = $parser->getPayload();
        $providedSignature = (string)$parser->getSignature();

        if (!is_array($providedPayload)){
            throw new \Exception('Payload hash is invalid.');
        }

        $serverCreatedSignature =  $this->sign($providedPayload);

        $parser->verifyPayload();

        if (empty($serverCreatedSignature) || empty($providedSignature)){
            throw new \Exception('Signature missing.');
        }

        if (\hash_equals($serverCreatedSignature, $providedSignature) === true){
            return $providedPayload;
        }

        throw new \Exception('Signature not valid.');
    }
}
