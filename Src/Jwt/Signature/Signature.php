<?php

namespace Rs\Jwt\Signature;

use Rs\Jwt\SignatureInterface;
use Rs\Jwt\SignatureTrait;


/**
 * Class Signature
 * @package Rs\Jwt\Signature
 */
abstract class Signature implements SignatureInterface
{

    use SignatureTrait;

    /**
     * @var array
     */
    protected $header;

    /**
     * @var string
     */
    protected $active;

    /**
     * @var array
     */
    protected $algos;

    /**
     * @param string $algo
     */
    public function setActiveAlgo($algo){

        $active = strtoupper($algo);

        if (isset($this->algos[$active])){
            $this->header['alg'] = $active;
            $this->active = $this->algos[$active];
        }
    }

    public function getHeader()
    {
        return $this->header;
    }

    public function setHeader($key, $value)
    {
        $this->header[strtolower($key)] = $value;
    }

    public function getAlgos()
    {
        return $this->algos;
    }
}
