<?php

namespace Rs\Jwt;

/**
 * Class JWT
 *
 * @package Rs\Jwt
 */
class JWT implements JwtInterface
{
    /**
     * @var SignatureInterface
     */
    private $signature;

    /**
     * @var ParserInterface
     */
    private $parser;

    /**
     * @var array
     */
    private $signatures = [];

    /**
     * Token constructor.
     * @param ParserInterface $tokenParser
     */
    public function __construct(ParserInterface $tokenParser)
    {
        $this->parser = $tokenParser;
    }

    /**
     * @inheritdoc
     */
    public function withSignature($reference, SignatureInterface $signature)
    {
        $new = clone $this;
        $new->signatures[$reference] = $signature;
        $new->signature = $signature;
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function useSignature($algorithm)
    {
        $new = clone $this;
        $foundSig = false;
        $signatureType = false;
        $signatureHeader = false;
        $signatureName = false;

        /**
         * @var SignatureInterface $sig
         */
        foreach ($new->signatures as $sigName => $sig) {
            $AlgoType = $sig->getAlgos();

            if (array_key_exists($algorithm, $AlgoType)) {
                $foundSig = true;
                $signatureType = $sigName;
                $signatureHeader = $AlgoType[$algorithm];
                $signatureName = key($AlgoType);
            }
        }

        if ($foundSig) {
            $new->signature = $new->signatures[$signatureType];
            $new->signature->setHeader('alg', $signatureName);
            $new->signature->setActiveAlgo($signatureHeader);
        } else {
            throw new \Exception('Unable to find correct algorithm');
        }

        return $new;
    }

    /**
     * @inheritdoc
     */
    public function import($token)
    {
        $new = clone $this;
        $new->parser = $new->parser->import($token);
        $new->useSignature($new->parser->getHeader('alg'));
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function createFromArray($algo, array $claims)
    {
        $new = clone $this;
        $new->useSignature($algo);
        $new->parser = $new->parser->setClaims($claims);
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function verify()
    {
        return $this->signature->verify(
            $this->parser
        );
    }

    /**
     * @inheritdoc
     */
    public function getToken()
    {
        $rawPayload = $this->parser->getClaims();

        $header = $this->parser->pack($this->signature->getHeader());
        $payload = $this->parser->pack($rawPayload);
        $signature = base64_encode($this->signature->sign($rawPayload));

        return $header.'.'.$payload.'.'.$signature;
    }

    /**
     * @inheritdoc
     */
    public function withId($id)
    {
        $new = clone $this;
        $new->parser->setClaim('jti', $id);
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function withExpires($timestamp)
    {
        $new = clone $this;
        $new->parser->setClaim('exp', $timestamp);
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function withIssuer($issuer)
    {
        $new = clone $this;
        $new->parser->setClaim('iss', $issuer);
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function withIssuedAt($timestamp)
    {
        $new = clone $this;
        $new->parser->setClaim('iat', $timestamp);
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function withNotBefore($timestamp)
    {
        $new = clone $this;
        $new->parser->setClaim('nbf', $timestamp);
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function withAudience($domain)
    {
        $new = clone $this;
        $new->parser->setClaim('aud', $domain);
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function withSubject(array $subject)
    {
        $new = clone $this;
        $new->parser->setClaim('sub', $subject);
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function withClaim($claim, $data)
    {
        $new = clone $this;
        $new->parser->setClaim($claim, $data);
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function getParser()
    {
        return $this->parser;
    }
}
