<?php

namespace Rs\Jwt;

/**
 * Class Validator
 * @package Rs\Jwt
 */
class Validator implements ValidatorInterface
{
    /**
     * Validation rules.
     * @var array
     */
    private $rules = [];

    /**
     * Import rules.
     * @param array $rules
     * @return ValidatorInterface
     */
    public function withRules(array $rules){
        $new = clone $this;
        $new->rules = $rules;
        return $new;
    }

    /**
     * with id string.
     * @param string $id
     * @return Validator
     */
    public function withId($id){
        $new = clone $this;
        $new->rules['jti'] = $id;
        return $new;
    }

    /**
     * with expected issuer claim.
     * @param string $issuer
     * @return Validator
     */
    public function withIssuer($issuer){
        $new = clone $this;
        $new->rules['iss'] = $issuer;
        return $new;
    }

    /**
     * with expected audience claim.
     * @param string $audience
     * @return Validator
     */
    public function withAudience($audience){
        $new = clone $this;
        $new->rules['aud'] = $audience;
        return $new;
    }

    /**
     * with expected subject claim.
     * @param array $subject
     * @return Validator
     */
    public function withSubject(array $subject){
        $new = clone $this;
        $new->rules['sub'] = $subject;
        return $new;
    }

    /**
     * with not before timestamp.
     * @param int $timestamp
     * @return Validator
     */
    public function withNotBefore($timestamp){
        $new = clone $this;
        $new->rules['nbf'] = $timestamp;
        return $new;
    }

    /**
     * With expires timestamp.
     * @param int $timestamp
     * @return Validator
     */
    public function withExpires($timestamp){
        $new = clone $this;
        $new->rules['exp'] = $timestamp;
        return $new;
    }

    /**
     * with issued at timestamp.
     * @param int $timestamp
     * @return Validator
     */
    public function withIssuedAt($timestamp){
        $new = clone $this;
        $new->rules['iat'] = $timestamp;
        return $new;
    }

    /**
     * with custom expected claim rule.
     * @param string $header Claim header.
     * @param string|array $value Expected Claim.
     * @return Validator
     */
    public function withClaim($header, $value){
        $new = clone $this;
        $new->rules[$header] = $value;
        return $new;
    }

    /**
     * Returns number of added rules.
     * @return int
     */
    public function getTotalRules(){
        return \count($this->rules);
    }

    /**
     * Returns added rules.
     * @return array
     */
    public function getRules(){
        return $this->rules;
    }

    /**
     * Return rule by header name.
     * @param string $name
     * @return string|array|null
     */
    public function getRule($name){

        if (isset($this->rules[$name])){
            return $this->rules[$name];
        }
        return null;
    }

    public function clearRules(){
        $this->rules = [];
    }

}
