<?php

namespace Rs\Jwt;

/**
 * Json Web Token Interface.
 *
 * @package Rs\Jwt
 */
interface JwtInterface
{
    /**
     * Add signature and reference to collection.
     *
     * @param string $reference Signature reference.
     * @param SignatureInterface $signature Signature Class.
     * @return JwtInterface
     */
    public function withSignature($reference, SignatureInterface $signature);

    /**
     * Use signature algorithm from collection.
     *
     * @param string $algorithm Signature reference
     * @return JwtInterface
     * @throws \Exception
     */
    public function useSignature($algorithm);

    /**
     * Import token.
     *
     * @param string $token Token string.
     * @return JwtInterface
     * @throws \Exception
     */
    public function import($token);

    /**
     * Create Token from claims.
     *
     * @param string $algo
     * @param array $claims
     * @return JwtInterface
     * @throws \Exception
     */
    public function createFromArray($algo, array $claims);

    /**
     * Returns token string.
     *
     * @return string
     */
    public function getToken();

    /**
     * Verify token.
     *
     * @return bool
     * @throws \Exception
     */
    public function verify();

    /**
     * With id claim.
     *
     * @param string $id
     * @return JwtInterface
     */
    public function withId($id);

    /**
     * With expires claim.
     *
     * @param $timestamp
     * @return JwtInterface
     */
    public function withExpires($timestamp);

    /**
     * With issuer claim.
     *
     * @param $issuer
     * @return JwtInterface
     */
    public function withIssuer($issuer);

    /**
     * With issued at claim.
     *
     * @param $timestamp
     * @return JwtInterface
     */
    public function withIssuedAt($timestamp);

    /**
     * With not before claim.
     *
     * @param $timestamp
     * @return JwtInterface
     */
    public function withNotBefore($timestamp);

    /**
     * With audience claim.
     *
     * @param $domain
     * @return JwtInterface
     */
    public function withAudience($domain);

    /**
     * With subject claim.
     *
     * @param array $subject
     * @return JwtInterface
     */
    public function withSubject(array $subject);

    /**
     * With custom claim.
     *
     * @param string $claim
     * @param $data
     * @return JwtInterface
     */
    public function withClaim($claim, $data);

    /**
     * Get token parser.
     *
     * @return ParserInterface
     */
    public function getParser();

}
