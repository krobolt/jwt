<?php

namespace Rs\Jwt;

/**
 * Class JwtTrait
 * @package Rs\Jwt
 */
trait SignatureTrait{

    /**
     * Returns bas64 encoded string.
     * @param array $data
     * @return string
     */
    public function pack(array $data = []){
        return \base64_encode(
            \json_encode($data)
        );
    }

    /**
     * Decodes base64 encoded string.
     * @param string $string
     * @return array
     */
    public function unpack($string){
        $json = \base64_decode($string);
        $array = \json_decode($json,true);
        return $array;
    }
}
