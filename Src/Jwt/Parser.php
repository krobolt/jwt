<?php

namespace Rs\Jwt;
use Exception;

/**
 * Class Parser
 * @package Rs\Jwt
 */
class Parser implements ParserInterface
{
    use SignatureTrait;

    const SUPPORTED = ['HS256','HS384','HS512','RS256','RS384','RS512','ES256','ES384','ES512'];

    /**
     * Imported header.
     * @var array
     */
    private $header = [];

    /**
     * Imported payload.
     * @var array
     */
    private $payload = [];

    /**
     * Imported signature.
     * @var string
     */
    private $signature;

    /**
     * Total validated claims.
     * @var int
     */
    private $validatedClaims = 0;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * Import token string.
     *
     * @param string $token
     * @return Parser
     * @throws Exception
     */
    public function import($token){

        $new = clone $this;

        $jwtReq = explode('.',$token,3);

        if(!isset($jwtReq[2])){
            throw new \Exception('Token not valid.');
        }

        $new->token = $token;
        $new->header =    $this->unpack($jwtReq[0]);
        $new->payload =   $this->unpack($jwtReq[1]);
        $new->signature = base64_decode($jwtReq[2]);

        $new->processHeader();

        return $new;
    }

    public function getToken(){
        return $this->token;
    }

    /**
     * Returns header.
     * Get header by name
     * @param $name
     * @return string|array|null
     */
    public function getHeader($name){
        if (isset( $this->header[$name] )){
            return $this->header[$name];
        }
        return null;
    }

    public function setClaims(array $claims){
        $new = clone $this;
        $new->payload = $claims;
        return $new;
    }

    /**
     * Return all token headers.
     * @return array
     */
    public function getHeaders(){
        return $this->header;
    }

    /**
     * Returns payload claim value.
     * @param $header
     * @return string|array|null
     */
    public function getClaim($header){
        if(isset($this->payload[$header])){
            return $this->payload[$header];
        }
        return null;
    }

    /**
     * Returns payload claims.
     * @return array
     */
    public function getClaims(){
        return $this->payload;
    }

    /**
     * Set claim.
     * @param $header
     * @param $value
     * @return void
     */
    public function setClaim($header, $value){
        $this->payload[$header] = $value;
    }

    /**
     * Returns imported token signature.
     * @return string
     */
    public function getSignature(){
        return $this->signature;
    }

    /**
     * Process Headers
     * @throws Exception
     */
    public function processHeader(){

        if( !isset($this->header['typ'])){
            throw new \Exception('Header Type not specified');
        }

        if (strtoupper($this->header["typ"]) !== 'JWT'){
            throw new \Exception('Header does not container Json Web Token Type');
        }

        if( !isset($this->header['alg'])){
            throw new \Exception('Algo not defined');
        }

        if (!in_array(strtoupper($this->header['alg']), self::SUPPORTED)){
            throw new \Exception('Algo not valid');
        }

        return;
    }

    /**
     * @param string $name
     * @return true
     * @throws Exception
     */
    public function validateClaim($name){

        $claimRule = $this->validator->getRule($name);

        if ( !isset($claimRule ) ) {
            throw new \Exception("Validation for claim {$name} can not be found.");
        }

        $expected = json_encode($claimRule);
        $actual = json_encode($this->payload[$name]);
        if ($expected === $actual){
            $this->validatedClaims++;
            return true;
        }
        throw new \Exception("Claim: {$name} does not match. Expected: {$expected} Actual:{$actual}");
    }

    /**
     * Process payload claims.
     *
     * @throws Exception
     */
    public function verifyPayload(){
        foreach ($this->payload as $item => $value){
            switch ($item){
                case 'exp':
                    $this->checkExpires($value);
                    break;
                case 'nbf':
                    $this->checkNotBefore($value);
                    break;
                case 'iat':
                    $this->checkIssuedAt($value);
                    break;
            }
        }
    }

    /**
     * Check that all claims have been validated.
     * @throws Exception
     */
    public function checkTotalClaims(){
        if ($this->validatedClaims !== $this->validator->getTotalRules()){
            throw new \Exception('Missing validation rules, not all claims have been validated.');
        };
    }

    /**
     * Check that tokens are issued before 'now'.
     * @throws Exception
     */
    public function checkIssuedAt($timestamp){
        if ($timestamp > time()){
            throw new \Exception('Tokens issued at error. Tokens cannot be issued in the future.');
        }
        $this->validatedClaims++;
    }

    /**
     * @param int $timestamp
     * @return bool
     * @throws Exception
     */
    public function checkNotBefore($timestamp){
        if ($timestamp < time()){
            throw new \Exception('Token not before time invalid. Tokens has expired.');
        }
        $this->validatedClaims++;
        return true;
    }

    /**
     * @param int $timestamp
     * @return bool
     * @throws Exception
     */
    public function checkExpires($timestamp){
        if ( ($timestamp - time()) < 0){
            throw new \Exception('Token has expired');
        }
        $this->validatedClaims++;
        return true;
    }

    /**
     * Return token payload.
     *
     * @return array
     */
    public function getPayload(){
        return $this->payload;
    }

}
