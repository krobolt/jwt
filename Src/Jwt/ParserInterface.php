<?php

namespace Rs\Jwt;

/**
 * Interface ParserInterface
 * @package Rs\Jwt
 */
interface ParserInterface
{
    /**
     * Import token string.
     * @param string $token
     * @throws \Exception
     */
    public function import($token);

    /**
     * Return header value.
     * Get header by name
     * @param $name
     * @return string|array|null
     */
    public function getHeader($name);

    /**
     * Returns all token headers.
     * @return array
     */
    public function getHeaders();

    /**
     * Return token payload.
     *
     * @return array
     */
    public function getPayload();


    public function verifyPayload();

    /**
     * Returns payload claim value.
     * @param $header
     * @return string|array|null
     */
    public function getClaim($header);

    /**
     * Returns payload claims.
     * @return array
     */
    public function getClaims();

    /**
     * Set claim payload.
     * @param array $claims
     */
    public function setClaims(array $claims);

    /**
     * Set claim.
     * @param $header
     * @param $value
     * @return void
     */
    public function setClaim($header, $value);

    /**
     * Returns imported token signature.
     * @return string
     */
    public function getSignature();

    /**
     * Returns bas64 encoded string.
     * @param array $data
     * @return string
     */
    public function pack(array $data = []);

    /**
     * Decodes base64 encoded string.
     * @param string $string
     * @return array
     */
    public function unpack($string);
}
