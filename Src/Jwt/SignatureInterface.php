<?php

namespace Rs\Jwt;

/**
 * Interface SignatureInterface
 *
 * @package Rs\Jwt
 */
interface SignatureInterface
{
    /**
     * Return signed signature.
     * @param array $payload
     * @return string
     * @throws \Exception
     */
    public function sign(array $payload);

    /**
     * Verify unknown signature string against payload.
     *
     * @param ParserInterface $parser
     * @return mixed
     * @throws \Exception
     */
    public function verify(ParserInterface $parser);

    /**
     * Set active algorithm.
     * @param string $algo
     * @return void
     */
    public function setActiveAlgo($algo);

    /**
     * Return signature header.
     * @return array
     */
    public function getHeader();

    /**
     * Set header value.
     * @param string $key
     * @param mixed $value
     */
    public function setHeader($key, $value);

    /**
     * @return array
     */
    public function getAlgos();
}