<?php

namespace Rs\Jwt;

/**
 * Interface ValidatorInterface
 * @package Rs\Jwt
 */
interface ValidatorInterface
{
    /**
     * with custom expected claim rule.
     * @param string $header Claim header.
     * @param string|array $value Expected Claim.
     * @return $this
     */
    public function withClaim($header, $value);

    /**
     * Returns number of added rules.
     * @return int
     */
    public function getTotalRules();
    /**
     * Returns added rules.
     * @return array
     */
    public function getRules();

    /**
     * Return rule by header name.
     * @param string $name
     * @return string|array|null
     */
    public function getRule($name);

    /**
     * Import rules.
     * @param array $rules
     * @return ValidatorInterface
     */
    public function withRules(array $rules);
}
